'use strict';
let $newsWrapper;

(() => {
    $newsWrapper = $('.news-wrapper');
    if ($newsWrapper.length) {
        const $searchInput = $('.search');
        performSearch();

        $searchInput.on('change', () => {
            const searchString = $searchInput.val();
            performSearch(searchString);
        });
    }

    const $submitButton = $('.submit-button');

    if ($submitButton.length) {
        const $nameField = $('.name');
        const $emailField = $('.email');
        const $messageField = $('.message');
        const $nameLabel = $('.name-label');
        const $messageLabel = $('.message-label');
        const $emailLabel = $('.email-label');
        const emailRegex = new RegExp('^\\S+@\\S+$');

        $submitButton.on('click', () => {
            $nameLabel.toggleClass('hidden', !!$nameField.val());
            $emailLabel.toggleClass('hidden', !!$emailField.val() && emailRegex.test($emailField.val()));
            $messageLabel.toggleClass('hidden', !!$messageField.val());

            if (!$('label').is(':visible')) {
                alert(`Спасибо, ${$nameField.val()}! Ваше сообщение отправлено. Мы свяжемся с вами по Email (${$emailField.val()}) в ближайшее время.`);
                $nameField.val('');
                $emailField.val('');
                $messageField.val('');
            }
        })
    }
})();

function performSearch(searchString) {
    const URLPrefix = 'https://newsapi.org/v2/';
    const URLPostfix = '&apiKey=7a9819d8b8974386b68abbf47b751d8f';
    const endpoint = searchString ? `everything?q=${searchString}` : 'top-headlines?country=us';
    const url = URLPrefix + endpoint + URLPostfix;

    $.ajax(url).then((data) => {
        $('.news-item').remove();
        data.articles.forEach((article) => {
            const $newsItem = `<div class='news-item'>
                    <div class="title">${article.title}</div>
                    <div class="author">Author: ${article.author}, ${new Date(article.publishedAt).toLocaleDateString()}</div>
                    <img class="image" src="${article.urlToImage}"/>                         
                    <div class="description">${article.description}<a href="${article.url}" class="more">Read more.</a></div>                                        
                </div>`;
            $newsWrapper.append($newsItem)
        })
    })
}