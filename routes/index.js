'use strict';

module.exports = {
    getHomePage: (req, res) => {
        let query = "SELECT * FROM `posts` ORDER BY id ASC";

        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('index.ejs', {
                title: "Welcome to my Blog",
                posts: result
            });
        });
    },
    getNewsPage: (req, res) => {
        res.render('news.ejs', {
            title: "RSS News"
        });
    },
    getMapPage: (req, res) => {
        res.render('map.ejs', {
            title: "Travel Map"
        });
    },
    getContactPage: (req, res) => {
        res.render('contact.ejs', {
            title: "Contact us"
        });
    }
};